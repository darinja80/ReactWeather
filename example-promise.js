function addPromise (a, b) {
  return new Promise(function (resolve, reject) {
    if(typeof a === "number" && typeof b === "number" ) {

      setTimeout(function() {
        resolve(a + b);
      }, 1000);
    }
    else {
      reject("Both items need to be numbers");
    }
  });
}

addPromise(4, 175).then(function(sum)  {
  console.log(sum);
},
function (err) {
  console.log("promise error", err);
});
