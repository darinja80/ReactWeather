// var names = ['Darin', 'Julie', 'Thomas'];
//
// names.forEach((name) => {
//   console.log('arrowFunc', name);
// });
//
// names.forEach((name) => console.log('arrowFunc2', name));

// var returnMe = (name) => name + '!';
//
// console.log(returnMe('Darin'));

// var person = {
//   name: 'Andrew',
//   greet: function() {
//     names.forEach((name) => {
//       console.log(this.name + ' says hi to ' + name)
//     });
//   }
// };
//
// person.greet();

function add (a, b) {
  return a + b;
}

var addExpression = (a, b) => a + b;

console.log(addExpression(1, 4));

var addStatement = (a, b) => {
  return(a + b);
}

console.log(addStatement(6, 14));

console.log(add(1, 3));
console.log(add(9, 0));
